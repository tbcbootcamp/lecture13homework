package com.example.lecture13homework

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val albumList = mutableListOf<UserModel>()
    private lateinit var adapter: RecyclerViewAdapter
    private val requestCode = 69

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        adapter = RecyclerViewAdapter(albumList, object : ItemOnClick {
            override fun onClick(position: Int) {
                removeAlbum(position)
            }
        })
        recycleView.layoutManager = LinearLayoutManager(this)
        recycleView.adapter = adapter

        setData()

        addButton.setOnClickListener {
            val intent = Intent(this, NewAlbumActivity::class.java)
            startActivityForResult(intent, requestCode)
        }
    }

    private fun setData() {
        albumList.add(
            0,
            UserModel(
                "Queen",
                "The Game",
                "1980",
                R.drawable.ic_library_music_black_24dp
            )
        )
        albumList.add(
            0,
            UserModel(
                "Pink Floyd",
                "The Wall",
                "1979",
                R.drawable.ic_library_music_black_24dp
            )
        )
        albumList.add(
            0,
            UserModel(
                "Led Zeppelin",
                "Mothership",
                "2007",
                R.drawable.ic_library_music_black_24dp
            )
        )
        albumList.add(
            0,
            UserModel(
                "Deep Purple",
                "Stormbringer",
                "1974",
                R.drawable.ic_library_music_black_24dp
            )
        )
        albumList.add(
            0,
            UserModel(
                "The doors",
                "L.A. Woman",
                "1971",
                R.drawable.ic_library_music_black_24dp
            )
        )

    }

    private fun removeAlbum(position: Int) {
        val alert = AlertDialog.Builder(this)
        alert.setTitle("Remove User")
        alert.setMessage("Are you sure you want to remove user?")
        alert.setPositiveButton(
            "Yes"
        ) { _: DialogInterface, _: Int ->
            albumList.removeAt(position)
            adapter.notifyItemRemoved(position)
        }
        alert.setNegativeButton("No") { _: DialogInterface, _: Int ->
        }
        alert.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == requestCode) {
            val bandName = data!!.extras!!.getString("band")
            val albumName = data.extras!!.getString("album")
            val releaseDate = data.extras!!.getString("releaseDate")

            albumList.add(
                0,
                UserModel(
                    bandName!!,
                    albumName!!,
                    releaseDate!!,
                    R.drawable.ic_library_music_black_24dp
                )
            )
            adapter.notifyItemInserted(0)
            recycleView.scrollToPosition(0)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}

