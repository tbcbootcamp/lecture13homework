package com.example.lecture13homework

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.add_album_layout.*

class NewAlbumActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_album_layout)
        init()
    }

    private fun init() {
        addAlbumButton.setOnClickListener {
            if (bandNameEditText.text.isNotEmpty() && albumNameEditText.text.isNotEmpty() && releaseDateEditText.text.isNotEmpty()) {
                addAlbumInfo()
            } else {
                Toast.makeText(applicationContext, "Please fill all fields!", Toast.LENGTH_SHORT).show()
            }

        }
    }

    private fun addAlbumInfo() {
        intent.putExtra("band", bandNameEditText.text.toString())
        intent.putExtra("album", albumNameEditText.text.toString())
        intent.putExtra("releaseDate", releaseDateEditText.text.toString())
        intent.putExtra("image", R.drawable.ic_library_music_black_24dp)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
